import json
import aiohttp_cors
from aiohttp import web


def JsonResp(data):
    return web.Response(text=json.dumps(data),
                        content_type='application/json')


async def bot_register(request):
    """
    Issue a new identity to the requesting bot.
    """
    ident = 'blah'
    return JsonResp({"ident": ident})


def run_server(host, port):
    print('Setting database')
    app = web.Application()
    print('Adding routes')
    app.router.add_get('/bot/register', bot_register)
    print('Adding cors')
    corsconfig = {"*": aiohttp_cors.ResourceOptions(allow_credentials=True,
                                                    expose_headers="*",
                                                    allow_headers="*")}
    cors = aiohttp_cors.setup(app, defaults=corsconfig)
    for route in list(app.router.routes()):
        cors.add(route)
    print('Running app')
    web.run_app(app, host=host, port=port)


if __name__ == '__main__':
    run_server('0.0.0.0', 8000)
